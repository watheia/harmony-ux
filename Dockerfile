# This file is a template, and might need editing before it works on your project.
FROM node:lts-alpine

#  use of `process.dlopen` is necessary
RUN apk add --no-cache libc6-compat \
    apt-transport-https \
    bash \
    gcc \
    git \
    git-lfs \
    make \
    python \
    g++

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

RUN yarn global add @teambit/bvm --unsafe-perm=true

USER node
ENV PATH "$PATH:$HOME/bin"
WORKDIR /home/node/app
COPY --chown=node . .

RUN bvm install && \
    bit init --harmony && \
    bit config set analytics_reporting false && \
    bit config set error_reporting false && \
    bit config set no_warnings true && \
    bit install && \
    bit environment

CMD ["bit", "--help"]
